﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_SupplierTransDetails
/// </summary>
public class BL_SupplierTransDetails
{
	public BL_SupplierTransDetails()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int64 SupplierTransId { get; set; }
    public Int64 SupplierTransDetailsId { get; set; }
    public Int64 ItemId { get; set; }
    public Int64 Out { get; set; }
    public Int64 In { get; set; }
    public Boolean IsVerified { get; set; }
    public Boolean Status { get; set; }
}