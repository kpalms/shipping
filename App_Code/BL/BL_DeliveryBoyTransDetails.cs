﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_DeliveryBoyTransDetails
/// </summary>
public class BL_DeliveryBoyTransDetails
{
	public BL_DeliveryBoyTransDetails()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int64 DeliveryBoyTransDetailsId { get; set; }
    public Int64 DeliveryBoyTransId { get; set; }
    public Int64 ItemId { get; set; }
    public decimal RetailerComission { get; set; }
    public decimal RetailerRate { get; set; }
    public int FilledQty { get; set; }
    public int EmptyQty { get; set; }
    public int NewSaleQty { get; set; }
    public Boolean Status { get; set; }
}