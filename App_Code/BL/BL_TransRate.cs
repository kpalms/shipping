﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_TransRate
/// </summary>
public class BL_TransRate
{
	public BL_TransRate()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public Int64 TransRateId { get; set; }
    public DateTime DateTime { get; set; }
    public Int64 RetailerId { get; set; }
    public Int64 SupplierId { get; set; }
    public Int64 ManufacturerId { get; set; }

}