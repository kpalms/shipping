﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_InvoiceDetails
/// </summary>
public class BL_InvoiceDetails
{
	public BL_InvoiceDetails()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int64 InvoiceNo { get; set; }
    public Int64 RetailerId { get; set; }
    public Int64 ManufacturerId { get; set; }
    public Int64 SupplierId { get; set; }
    public string InvoiceDate { get; set; }
    public string FromDate { get; set; }
    public string ToDate { get; set; }
    public decimal TotalAmount { get; set; }
    public Boolean IsComplete { get; set; }
}