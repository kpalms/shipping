﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_Retailer
/// </summary>
public class BL_Retailer
{
	public BL_Retailer()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public Int64 RetailerId { get; set; }
    public DateTime DateTime { get; set; }
    public string ShopName { get; set; }
    public string ShopPicture { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string MobileNo { get; set; }
    public string EmailId { get; set; }
    public int CityId { get; set; }
    public int AreaId { get; set; }
    public string LankMark { get; set; }
    public string Address { get; set; }
    public string Pincode { get; set; }
    public string Password { get; set; }
    public Boolean IsDelete { get; set; }



}