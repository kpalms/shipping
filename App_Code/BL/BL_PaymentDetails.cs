﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_PaymentDetails
/// </summary>
public class BL_PaymentDetails
{
	public BL_PaymentDetails()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int64 InvoiceNo { get; set; }
    public Int64 PaymentId { get; set; }
    public string PaymentDate { get; set; }
    public decimal PaymentAmount { get; set; }
    public Boolean IsVerified { get; set; }
    public DateTime VerifiedDateTime { get; set; }
}