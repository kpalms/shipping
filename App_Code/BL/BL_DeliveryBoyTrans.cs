﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_DeliveryBoyTrans
/// </summary>
public class BL_DeliveryBoyTrans
{
	public BL_DeliveryBoyTrans()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public Int64 DeliveryBoyTransId { get; set; }
    public string Date { get; set; }
    public string Time { get; set; }
    public Int64 RetailerId { get; set; }
    public Int64 DeliveryBoyId { get; set; }
    public int OTP { get; set; }
    public Boolean IsApproved { get; set; }


}