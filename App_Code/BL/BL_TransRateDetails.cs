﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_TransRateDetails
/// </summary>
public class BL_TransRateDetails
{
	public BL_TransRateDetails()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int64 TransRateDetailsId { get; set; }
    public Int64 TransRateId { get; set; }
    public Int64 ItemId { get; set; }
    public decimal RetailerCommission { get; set; }
    public decimal RetailerRate { get; set; }
    public decimal OutSupplierRate { get; set; }
    public decimal ManufacturerRate { get; set; }
    public Boolean Status { get; set; }
    public Boolean IsDelete { get; set; }
}