﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_StockManagement
/// </summary>
public class BL_StockManagement
{
	public BL_StockManagement()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int64 StockId { get; set; }
    public Int64 RetailerId { get; set; }
    public Int64 ItemId { get; set; }
    public int FilledQty { get; set; }
    public int EmptyQty { get; set; }
    public Boolean Status { get; set; }

}