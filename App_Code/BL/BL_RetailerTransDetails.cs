﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_RetailerTransDetails
/// </summary>
public class BL_RetailerTransDetails
{
	public BL_RetailerTransDetails()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int64 RetailerTransDetailsId { get; set; }
    public Int64 RetailerTransId { get; set; }
    public Int64 ItemId { get; set; }
    public decimal ManufacturerRate { get; set; }
    public decimal OutSupplierRate { get; set; }
    public int DeliveredQty { get; set; }
    public int CollectedQty { get; set; }
    public Boolean Status { get; set; }
}