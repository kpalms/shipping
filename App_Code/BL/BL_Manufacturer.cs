﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_Manufacturer
/// </summary>
public class BL_Manufacturer
{
	public BL_Manufacturer()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int64 ManufacturerId { get; set; }
    public DateTime DateTime { get; set; }
    public string OrganisationName { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string MobileNo { get; set; }
    public string EmailId { get; set; }
    public int CityId { get; set; }
    public int AreaId { get; set; }
    public string LankMark { get; set; }
    public string Address { get; set; }
    public string Pincode { get; set; }
    public string Password { get; set; }
    public Boolean IsDelete { get; set; }
}