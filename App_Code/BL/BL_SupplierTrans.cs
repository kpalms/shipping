﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_SupplierTrans
/// </summary>
public class BL_SupplierTrans
{
	public BL_SupplierTrans()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int64 SupplierTransId { get; set; }
    public string Date { get; set; }
    public string Time { get; set; }
    public Int64 ManufacturerId { get; set; }
    public Int64 DriverId { get; set; }
    public Int64 WorkerId { get; set; }
    public string OutNote { get; set; }
    public string InNote { get; set; }
    public Boolean IsComplete { get; set; }
    public DateTime CompleteDateTime { get; set; }

}