﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_RetailerTrans
/// </summary>
public class BL_RetailerTrans
{
	public BL_RetailerTrans()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public Int64 RetailerTransId { get; set; }
    public string Date { get; set; }
    public string Time { get; set; }
    public Int64 RetailerId { get; set; }
    public Int64 SupplierId { get; set; }
    public Int64 ManufacturerId { get; set; }
    public string NoteBySupplier { get; set; }
    public string NoteByRetailer { get; set; }
    public Boolean IsApprovedBySupplier { get; set; }
    public Boolean IsApprovedByRetailer { get; set; }
    public Boolean IsComplete { get; set; }

}