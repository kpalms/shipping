﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BL_MSupplier
/// </summary>
public class BL_MSupplier
{
	public BL_MSupplier()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public Int64 SupplierId { get; set; }
    public DateTime DateTime { get; set; }
    public Int64 ManufacturerId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string MobileNo { get; set; }
    public string Role { get; set; }
    public string State { get; set; }
    public string City { get; set; }
    public string Address { get; set; }
    public string Password { get; set; }
    public string SupplierPicture { get; set; }
    public Boolean IsDelete { get; set; }


}