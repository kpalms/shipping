﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using MySql.Data.MySqlClient;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Configuration;



/// <summary>
/// Summary description for WSManufacturer
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 

 [System.Web.Script.Services.ScriptService]
public class WSManufacturer : System.Web.Services.WebService {

    public WSManufacturer () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    [WebMethod]
    public string ManufacturerCrud(string ManufacturerId, string DateTime, string OrganisationName, string FirstName, string LastName, string MobileNo, string EmailId, string CityId, string AreaId, string LankMark, string Address, string Pincode, string Password, string IsDelete, string status)
    {

        string msg = "false";
        String strConnString = ConfigurationManager
.ConnectionStrings["connectionString"].ConnectionString;
        using (MySqlConnection con = new MySqlConnection(strConnString))
        {
            con.Open();


            if (HttpContext.Current.Session["OrgId"] != null)
            {
                MySqlCommand cmd = new MySqlCommand("ManufacturerCrud", con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (status == "INSERT")
                {
                    cmd.Parameters.AddWithValue("@Mode", ManufacturerId);
                    cmd.Parameters.AddWithValue("@OrgId", DateTime);
                    cmd.Parameters.AddWithValue("@ManageStockId", OrganisationName);
                    cmd.Parameters.AddWithValue("@ItemId", FirstName);
                    cmd.Parameters.AddWithValue("@Qty", LastName);
                    cmd.Parameters.AddWithValue("@Rate", MobileNo);
                    cmd.Parameters.AddWithValue("@Date", EmailId);
                    cmd.Parameters.AddWithValue("@CityId", CityId);
                    cmd.Parameters.AddWithValue("@AreaId", AreaId);
                    cmd.Parameters.AddWithValue("@LankMark", LankMark);
                    cmd.Parameters.AddWithValue("@Address", Address);
                    cmd.Parameters.AddWithValue("@Pincode", Pincode);
                    cmd.Parameters.AddWithValue("@Password", Password);
                    cmd.Parameters.AddWithValue("@IsDelete", IsDelete);

                }
                else if (status == "UPDATE")
                {

                    cmd.Parameters.AddWithValue("@Mode", ManufacturerId);
                    cmd.Parameters.AddWithValue("@OrgId", DateTime);
                    cmd.Parameters.AddWithValue("@ManageStockId", OrganisationName);
                    cmd.Parameters.AddWithValue("@ItemId", FirstName);
                    cmd.Parameters.AddWithValue("@Qty", LastName);
                    cmd.Parameters.AddWithValue("@Rate", MobileNo);
                    cmd.Parameters.AddWithValue("@Date", EmailId);
                    cmd.Parameters.AddWithValue("@CityId", CityId);
                    cmd.Parameters.AddWithValue("@AreaId", AreaId);
                    cmd.Parameters.AddWithValue("@LankMark", LankMark);
                    cmd.Parameters.AddWithValue("@Address", Address);
                    cmd.Parameters.AddWithValue("@Pincode", Pincode);
                    cmd.Parameters.AddWithValue("@Password", Password);
                    cmd.Parameters.AddWithValue("@IsDelete", IsDelete);
                }
                else if (status == "DELETE")
                {
                    cmd.Parameters.AddWithValue("@Mode", ManufacturerId);
                    cmd.Parameters.AddWithValue("@OrgId", DateTime);
                    cmd.Parameters.AddWithValue("@ManageStockId", OrganisationName);
                    cmd.Parameters.AddWithValue("@ItemId", FirstName);
                    cmd.Parameters.AddWithValue("@Qty", LastName);
                    cmd.Parameters.AddWithValue("@Rate", MobileNo);
                    cmd.Parameters.AddWithValue("@Date", EmailId);
                    cmd.Parameters.AddWithValue("@CityId", CityId);
                    cmd.Parameters.AddWithValue("@AreaId", AreaId);
                    cmd.Parameters.AddWithValue("@LankMark", LankMark);
                    cmd.Parameters.AddWithValue("@Address", Address);
                    cmd.Parameters.AddWithValue("@Pincode", Pincode);
                    cmd.Parameters.AddWithValue("@Password", Password);
                    cmd.Parameters.AddWithValue("@IsDelete", IsDelete);

                }
                cmd.ExecuteNonQuery();
                msg = "true";
            }
            else
            {

            }
            return msg;
        }

    }



    [WebMethod]
    public BL_Manufacturer[] SelectManufacturerDetails()
    {


        List<BL_Manufacturer> lst = new List<BL_Manufacturer>();
        DL_Connections dl_connection = new DL_Connections();
        DataTable ct = dl_connection.UseDataTablePro("SelectManufacturer");
        for (int i = 0; i < ct.Rows.Count; i++)
        {
            BL_Manufacturer obj = new BL_Manufacturer();
            obj.ManufacturerId = Convert.ToInt32(ct.Rows[i]["ManufacturerId"].ToString());
            
            //obj.DateTime = ct.Rows[i]["Date"].ToString();
            //obj.OrganisationName = Convert.ToInt32(ct.Rows[i]["Qty"].ToString());
            //obj.FirstName = ct.Rows[i]["ItemNames"].ToString();
            //obj.LastName = ct.Rows[i]["Status"].ToString();
            //obj.MobileNo = ct.Rows[i]["FirstName"].ToString();
            //obj.LastName = ct.Rows[i]["LastName"].ToString();
            //obj.MobileNo = ct.Rows[i]["MobileNo"].ToString();
            //obj.EmailId = ct.Rows[i]["Appartment"].ToString();
            //obj.CityId = ct.Rows[i]["Company"].ToString();
            //obj.AreaId = ct.Rows[i]["FlatNo"].ToString();
            //obj.LankMark = ct.Rows[i]["CityName"].ToString();
            //obj.Address = ct.Rows[i]["AreaName"].ToString();
            // obj.Pincode = ct.Rows[i]["CityName"].ToString();
            //obj.Password = ct.Rows[i]["AreaName"].ToString();
            //obj.IsDelete = ct.Rows[i]["FlatNo"].ToString();
            lst.Add(obj);
        }
        return lst.ToArray();
    }



    
}
