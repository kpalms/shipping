﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using MySql.Data.MySqlClient;
using System.Data;
using System.Web.Services;
using System.Web.Script.Services;
using System.Configuration;


/// <summary>
/// Summary description for DL_Connections
/// </summary>
public class DL_Connections
{
	public DL_Connections()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string constr = @"Data Source=182.50.133.78;Initial Catalog=DewDrinksdb;User Id=DewDrinks;Password=$iCo9m89;Convert Zero Datetime=True";


    MySqlConnection con = new MySqlConnection(constr);


    public void Open()
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
    }

    public void Close()
    {

        if (con.State == ConnectionState.Open)
        {
            con.Close();
        }

    }

    public DataTable UseDataTablePro(string SPName, params MySqlParameter[] parameters)
    {
        try
        {
            Open();

            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = con;
            if (parameters != null)
            {
                foreach (MySqlParameter item in parameters)
                    cmd.Parameters.Add(item);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "call " + SPName;
            MySqlDataAdapter da = new MySqlDataAdapter(cmd.CommandText, con);
            DataTable dt = new DataTable();
            

            da.Fill(dt);


            return dt;
        }
        catch (Exception)
        {

            throw;
        }
        finally
        {
            Close();
           
        }

    }



    public void UseExecuteNonQueryPro(string SPName, params MySqlParameter[] Parameters)
    {

        Open();
        MySqlCommand cmd = new MySqlCommand(SPName, con);

        cmd.CommandText = SPName;
        cmd.CommandType = CommandType.StoredProcedure;

        if (Parameters != null)
            foreach (MySqlParameter item in Parameters)
                cmd.Parameters.Add(item);


        cmd.ExecuteNonQuery();
        Close();

    }

    public string UseExcuteScallerPro(string Sname, params MySqlParameter[] parameters)
    {
        object id;
        Open();
        MySqlCommand cmd = new MySqlCommand(Sname, con);
        cmd.CommandType = CommandType.StoredProcedure;
        if (parameters != null)
        {
            foreach (MySqlParameter item in parameters)
                cmd.Parameters.Add(item);
        }

        try
        {
            id = cmd.ExecuteScalar();
            //dr = Convert.ToInt32(str);

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            Close();
        }

        return Convert.ToString(id);
    }

    public DataSet UseDatasetPro(string Sname, params MySqlParameter[] parameters)
    {
        DataSet ds = new DataSet();
        try
        {
            Open();
            MySqlCommand cmd = new MySqlCommand(Sname, con);
            cmd.CommandType = CommandType.StoredProcedure;
            if (parameters != null)
            {
                foreach (MySqlParameter item in parameters)
                    cmd.Parameters.Add(item);
            }
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(ds);
            Close();

            return ds;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }



}