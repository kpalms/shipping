﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/MasterPage.master" AutoEventWireup="true" CodeFile="AddManufacturer.aspx.cs" Inherits="Admin_AddManufacturer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    
<script type="text/javascript">
    var prodid = 0, opstatus = '';

    if (prodid != 0 && opstatus == 'UPDATE') {
        ManageStockId = prodid;
        status = opstatus;
    }


         function AddManufacturer(ManufacturerId, DateTime, OrganisationName, FirstName, LastName, MobileNo, EmailId, CityId, AreaId, LankMark, Address, Pincode, Password, IsDelete, status) {

             alert(status);

        if (prodid != 0 && opstatus == 'UPDATE') {
            ManageStockId = prodid;
            status = opstatus;
        }
        debugger

        $.ajax({

            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "http://localhost:1167/Shipping/WSManufacturer.asmx/ManufacturerCrud",
            data: "{'ManufacturerId':'" + ManufacturerId + "','DateTime':'" + DateTime + "','OrganisationName':'" + OrganisationName + "','FirstName':'" + FirstName + "','LastName':'" + LastName + "','MobileNo':'" + MobileNo + "','EmailId':'" + EmailId + "','CityId':'" + CityId + "','AreaId':'" + AreaId + "','LankMark':'" + LankMark + "','Address':'" + Address + "','Pincode':'" + Pincode + "','Password':'" + Password + "','IsDelete':'" + IsDelete + "','status':'" + status + "'}",
            dataType: "json",
            success: function (data) {

           
                if (data.d == 'true')
                    $('#ContentPlaceHolder1_txtFirstName').val("");
                   $('#ContentPlaceHolder1_txtLastName').val("");
               

            },
            error: function (data) {
                var r = data.responseText;
                var errorMessage = r.Message;
                alert(errorMessage);
            }
        });
    }


</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>Register New Manufacturer</strong></h3>
                </div>
                <div class="box-body">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Organisation Name</label>
                                <asp:TextBox ID="txtOrgaName" onblur="return errMessageClear()" class="form-control" placeholder="Enter Organisation Name" runat="server"></asp:TextBox>
                                <asp:Label ID="errOrgaName" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>


                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter First Name</label>
                                <asp:TextBox ID="txtFirstName" onblur="return errMessageClear()" class="form-control" placeholder="Enter First Name" runat="server"></asp:TextBox>
                                <asp:Label ID="errFirstName" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Last Name</label>
                                <asp:TextBox ID="txtLastName" onblur="return errMessageClear()" class="form-control" placeholder="Enter Last Name" runat="server"></asp:TextBox>
                                <asp:Label ID="errLastName" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>

                    </div>


                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Mobile No</label>
                                <asp:TextBox ID="txtMobileNo" onblur="return errMessageClear()" MaxLength="10" onkeypress="return isNumber(event)" class="form-control" placeholder="Enter Mobile No" runat="server"></asp:TextBox>
                                <asp:Label ID="errMobileNo" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Email Id</label>
                                <asp:TextBox ID="txtEmailId" onblur="return errMessageClear()" class="form-control" placeholder="Enter Email Id" runat="server"></asp:TextBox>
                                <asp:Label ID="errEmailId" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Select City</label>
                                <asp:DropDownList ID="ddlCity" onblur="return errMessageClear()" class="form-control" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="errCity" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Select Area</label>
                                <asp:DropDownList ID="ddlArea" onblur="return errMessageClear()" class="form-control" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="errArea" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>


                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Landmark</label>
                                <asp:TextBox ID="txtLankMark" onblur="return errMessageClear()" class="form-control" placeholder="Enter Landmark" runat="server"></asp:TextBox>
                                <asp:Label ID="errLankMark" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Pincode</label>
                                <asp:TextBox ID="txtPincode" onblur="return errMessageClear()" MaxLength="6" class="form-control" placeholder="Enter Pincode" runat="server"></asp:TextBox>
                                <asp:Label ID="errPincode" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>

                    </div>



                     <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Address</label>
                                <asp:TextBox ID="txtAddress" onblur="return errMessageClear()" TextMode="MultiLine" Rows="2" class="form-control" placeholder="Enter Address" runat="server"></asp:TextBox>
                                <asp:Label ID="errAddress" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>


                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter  Password</label>
                                <asp:TextBox ID="txtPassword" onblur="return errMessageClear()" TextMode="Password" class="form-control" placeholder="Enter  Password" runat="server"></asp:TextBox>
                                <asp:Label ID="errPassword" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Confirm Password</label>
                                <asp:TextBox ID="txtConfirPass" onblur="return errMessageClear()" TextMode="Password" class="form-control" placeholder="Enter Confirm Password" runat="server"></asp:TextBox>
                                <asp:Label ID="errConfirPass" class="help-block errorMessage" runat="server"></asp:Label>
                            </div>
                        </div>

                    </div>
                   
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1"></label>

<%--                                <asp:Button ID="btnRegister" OnClientClick="return FieldsValidations()" class="btn btn-block btn-primary" runat="server" Text="Register" OnClick="btnRegister_Click" />--%>


                                <input type="button" ID="btnRegister" class="btn btn-block btn-primary" runat="server" value="Register" onclick="AddManufacturer('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', 'INSERT')" />



                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            RequriedField = "This Field is Required.";


            FieldsValidations = function () {

                //txtOrgaName
                var txtOrgaName = $('#<%=txtOrgaName.ClientID%>').val();
                if (txtOrgaName == "") {
                    $('#<%=errOrgaName.ClientID%>').text(RequriedField);
                    $('#<%=txtOrgaName.ClientID%>').focus();
                    return false;
                }
                else {
                    $('#<%=errOrgaName.ClientID%>').text(null);
                }


                //txtFirstName
                var txtFirstName = $('#<%=txtFirstName.ClientID%>').val();
                if (txtFirstName == "") {
                    $('#<%=errFirstName.ClientID%>').text(RequriedField);
                    $('#<%=txtFirstName.ClientID%>').focus();
                    return false;
                }
                else {
                    $('#<%=errFirstName.ClientID%>').text(null);
                }


                //txtLastName
                var txtLastName = $('#<%=txtLastName.ClientID%>').val();
                if (txtLastName == "") {
                    $('#<%=errLastName.ClientID%>').text(RequriedField);
                    $('#<%=txtLastName.ClientID%>').focus();
                    return false;
                }
                else {
                    $('#<%=errLastName.ClientID%>').text(null);
                }


                //txtMobileNo
                var txtMobileNo = $('#<%=txtMobileNo.ClientID%>').val();
                if (txtMobileNo == "") {
                    $('#<%=errMobileNo.ClientID%>').text(RequriedField);
                    $('#<%=txtMobileNo.ClientID%>').focus();
                    return false;
                }
                else {
                    $('#<%=errMobileNo.ClientID%>').text(null);
                }

                //txtEmailId
                var txtEmailId = $('#<%=txtEmailId.ClientID%>').val();
                if (txtEmailId == "") {
                    $('#<%=errEmailId.ClientID%>').text(RequriedField);
                    $('#<%=txtEmailId.ClientID%>').focus();
                    return false;
                }
                else {
                    $('#<%=errEmailId.ClientID%>').text(null);
                }

            }

        });
    </script>

</asp:Content>

