﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Admin_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
 <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <noscript>
        <META HTTP-EQUIV="Refresh" CONTENT="0;URL=../ShowErrorPage.html">
    </noscript>
    <title>Dew Drinks Shipping | Log In</title>
 <link rel="shortcut icon" href="../dist/img/ddicon.jpg" type="image/png" />
   
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css" />
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../dist/css/CustomGridePager.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->
    <link href="../dist/css/CustomGridePager.css" rel="stylesheet" />
    <link href="../dist/css/MyCss.css" rel="stylesheet" />
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" />
    <link rel="stylesheet" href="../dist/css/skins/skin-blue.min.css" />
    <link href="../dist/css/ListViewPaginationCss.css" rel="stylesheet" />
    <link href="../dist/css/MLMcss.css" rel="stylesheet" />

</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <b><a href="Login.aspx">Dew Drinks Shipping</a> </b>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Log In to start your session</p>
            <form id="form1"  class="cmxform" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                       
                         <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group has-feedback">
                                    <label for="exampleInputEmail">Enter Mobile Number</label>
                                        <asp:TextBox ID="txtMobileNo" class="form-control" MaxLength="10"  onblur="return errMessageClear()" onkeypress="return isNumber(event)" placeholder="Enter Mobile Number" runat="server"></asp:TextBox>
                                        <span class="fa  fa-mobile form-control-feedback"></span>
                                     <asp:Label ID="errMobileNo" class="help-block errorMessage" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group has-feedback">
                                    <label for="exampleInputEmail">Enter  Password</label>
                                    <asp:TextBox ID="txtPassword" class="form-control" MaxLength="15" title="Must be at least 8 characters , At least 1 number, 1 lowercase, 1 uppercase letter, At least 1 special character from @#$%&." onblur="return errMessageClear()" TextMode="Password" placeholder="Enter  Password" runat="server"></asp:TextBox>
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    <asp:Label ID="errPassword" class="help-block errorMessage" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                       

                     
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <asp:Button ID="btnLogin" class="btn btn-info btn-block btn-flat" OnClientClick="return FieldsValidations()"  runat="server" Text="Log In"  />
                            </div>
                            

                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                 <div class="form-group has-feedback ">
                                     <a href="ForgetPassword.aspx" class="text-right">I forgot my password</a> |
                                      <a href="Customer/Register.aspx" class="text-right">Register a new Manufacturer</a>
                                     </div>
                        </div>
                             </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </form>
        </div>
    </div>
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../dist/js/MyScript.js"></script>
    



    <script type="text/javascript">
        $(document).ready(function () {

            RequriedField = "This Field is Required.";

            FieldsValidations = function () {




                //txtMobileNo
                var txtMobileNo = $("#txtMobileNo").val();
                if (txtMobileNo == "") {
                    $("#errMobileNo").text(RequriedField);
                    $("#txtMobileNo ").focus();
                    return false;
                }
                else {
                    if (txtMobileNo.length == 10) {
                        $("#errMobileNo").text(null);
                    }
                    else {
                        $("#errMobileNo ").text("Enter 10 Digit Mobile Number.");
                        $("#txtMobileNo ").focus();
                        return false;
                    }
                }



                if (txtMobileNo.length == 10) {
                    $.ajax({
                        type: "POST",
                        url: "../Services/WebService1.asmx/RetrunMobileNo",
                        data: "{ MobileNo: '" + txtMobileNo + "' }",
                        contentType: "application/json; charset=utf-8",
                        datatype: 'json',
                        success: function (data) {
                            if (data.d == "0") {
                                $("#errMobileNo").text("Mobile Number is Not Found.");
                                $("#txtMobileNo ").focus();
                                return false;
                            }
                            else {
                                $("#errMobileNo").text(null);
                                return true;
                            }
                        },
                    });
                }

               

                //txtPassword
                var txtPassword = $("#txtPassword").val();
                if (txtPassword == "") {
                    $("#errPassword").text(RequriedField);
                    $("#txtPassword ").focus();
                    return false;
                }
               

                return true;

            }

            errMessageClear = function () {


                //txtMobileNo
                var txtMobileNo = $("#txtMobileNo").val();
                if (txtMobileNo != "") {
                    if (txtMobileNo.length == 10) {
                        $("#errMobileNo").text(null);

                        $.ajax({
                            type: "POST",
                            url: "../Services/WebService1.asmx/RetrunMobileNo",
                            data: "{ MobileNo: '" + txtMobileNo + "' }",
                            contentType: "application/json; charset=utf-8",
                            datatype: 'json',
                            success: function (data) {
                                if (data.d == "0") {
                                    $("#errMobileNo").text("Mobile Number is Not Found.");
                                    $("#txtMobileNo ").focus();
                                    return false;
                                }
                                else {
                                    $("#errMobileNo").text(null);
                                    return true;
                                }
                            },
                            error: function (r) {
                                alert(r.responseText);
                            },
                            failure: function (r) {
                                alert(r.responseText);
                            }
                        });
                    }
                    else {
                        $("#errMobileNo ").text(null);
                        $("#errMobileNo ").text("Enter 10 Digit Mobile Number.");
                        $("#txtMobileNo ").focus();
                    }
                }

              
                //txtPassword
                var txtPassword = $("#txtPassword").val();
                if (txtPassword != "") {

                    $("#errPassword").text(null);
                }


              

                return true;
            }
        });


    </script>

</body>
</html>